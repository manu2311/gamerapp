﻿
namespace GamerSA
{
    partial class FDetalls
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lb_id = new System.Windows.Forms.Label();
            this.lb_descripcio = new System.Windows.Forms.Label();
            this.lb_nom = new System.Windows.Forms.Label();
            this.lb_plataforma = new System.Windows.Forms.Label();
            this.lb_genere = new System.Windows.Forms.Label();
            this.lb_preu = new System.Windows.Forms.Label();
            this.lb_franjaEdat = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(116, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(552, 358);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(76, 398);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Id:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(76, 440);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nom:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(76, 492);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Descripció:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(685, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Plataforma:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(667, 550);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Genere:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label6.Location = new System.Drawing.Point(685, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "Preu:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(667, 579);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 16);
            this.label7.TabIndex = 7;
            this.label7.Text = "Franja Edat:";
            // 
            // lb_id
            // 
            this.lb_id.AutoSize = true;
            this.lb_id.Font = new System.Drawing.Font("Ebrima", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_id.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lb_id.Location = new System.Drawing.Point(145, 395);
            this.lb_id.Name = "lb_id";
            this.lb_id.Size = new System.Drawing.Size(0, 20);
            this.lb_id.TabIndex = 8;
            // 
            // lb_descripcio
            // 
            this.lb_descripcio.AutoSize = true;
            this.lb_descripcio.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_descripcio.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lb_descripcio.Location = new System.Drawing.Point(76, 522);
            this.lb_descripcio.Name = "lb_descripcio";
            this.lb_descripcio.Size = new System.Drawing.Size(0, 17);
            this.lb_descripcio.TabIndex = 9;
            // 
            // lb_nom
            // 
            this.lb_nom.AutoSize = true;
            this.lb_nom.Font = new System.Drawing.Font("Ebrima", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_nom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.lb_nom.Location = new System.Drawing.Point(143, 428);
            this.lb_nom.Name = "lb_nom";
            this.lb_nom.Size = new System.Drawing.Size(0, 32);
            this.lb_nom.TabIndex = 10;
            // 
            // lb_plataforma
            // 
            this.lb_plataforma.AutoSize = true;
            this.lb_plataforma.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_plataforma.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lb_plataforma.Location = new System.Drawing.Point(767, 150);
            this.lb_plataforma.Name = "lb_plataforma";
            this.lb_plataforma.Size = new System.Drawing.Size(0, 29);
            this.lb_plataforma.TabIndex = 11;
            this.lb_plataforma.Click += new System.EventHandler(this.lb_plataforma_Click);
            // 
            // lb_genere
            // 
            this.lb_genere.AutoSize = true;
            this.lb_genere.Font = new System.Drawing.Font("Ebrima", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_genere.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lb_genere.Location = new System.Drawing.Point(768, 545);
            this.lb_genere.Name = "lb_genere";
            this.lb_genere.Size = new System.Drawing.Size(0, 23);
            this.lb_genere.TabIndex = 12;
            // 
            // lb_preu
            // 
            this.lb_preu.AutoSize = true;
            this.lb_preu.Font = new System.Drawing.Font("Ebrima", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_preu.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lb_preu.Location = new System.Drawing.Point(754, 92);
            this.lb_preu.Name = "lb_preu";
            this.lb_preu.Size = new System.Drawing.Size(0, 46);
            this.lb_preu.TabIndex = 13;
            // 
            // lb_franjaEdat
            // 
            this.lb_franjaEdat.AutoSize = true;
            this.lb_franjaEdat.Font = new System.Drawing.Font("Ebrima", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_franjaEdat.ForeColor = System.Drawing.Color.Red;
            this.lb_franjaEdat.Location = new System.Drawing.Point(768, 574);
            this.lb_franjaEdat.Name = "lb_franjaEdat";
            this.lb_franjaEdat.Size = new System.Drawing.Size(0, 23);
            this.lb_franjaEdat.TabIndex = 14;
            // 
            // FDetalls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.ClientSize = new System.Drawing.Size(911, 646);
            this.Controls.Add(this.lb_franjaEdat);
            this.Controls.Add(this.lb_preu);
            this.Controls.Add(this.lb_genere);
            this.Controls.Add(this.lb_plataforma);
            this.Controls.Add(this.lb_nom);
            this.Controls.Add(this.lb_descripcio);
            this.Controls.Add(this.lb_id);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.MaximumSize = new System.Drawing.Size(929, 693);
            this.MinimumSize = new System.Drawing.Size(929, 693);
            this.Name = "FDetalls";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FDetalls";
            this.Load += new System.EventHandler(this.FDetalls_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lb_id;
        private System.Windows.Forms.Label lb_descripcio;
        private System.Windows.Forms.Label lb_nom;
        private System.Windows.Forms.Label lb_plataforma;
        private System.Windows.Forms.Label lb_genere;
        private System.Windows.Forms.Label lb_preu;
        private System.Windows.Forms.Label lb_franjaEdat;
    }
}
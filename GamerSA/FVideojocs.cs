﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GamerSA
{

    public partial class FVideojocs : Form
    {
        VideojocDAO videojocsDao = new VideojocDAO();
        List<Videojoc> listavideojocs = new List<Videojoc>();
        List<string> listaGeneresNoRepetidos = new List<string>();
        FDetalls detallsForm;

        public FVideojocs()
        {
            InitializeComponent();
            loadingVideojocs();
            loadingGeneres();
        }
        private void loadingGeneres()
        {

            List<string> listaGeneresCompleta = new List<string>();

            //creo una lista con todos los generos
            for (int i = 0; i < listavideojocs.Count; i++)
            {
                listaGeneresCompleta.Add(listavideojocs[i].Genere);               
            }
            //creo otra lista eliminando los generos repetidos
             listaGeneresNoRepetidos = listaGeneresCompleta.Distinct().ToList();

            //añado los items de la lista con los generos no repetidos al listbox.
            for (int i = 0; i < listaGeneresNoRepetidos.Count; i++)
            {
                listBox1.Items.Add(listaGeneresNoRepetidos[i]);
            }
        }
    


        private void FVideojocs_Load(object sender, EventArgs e)
        {          

        }

        private void loadingVideojocs()
        {         

            listavideojocs = videojocsDao.loadVideojocs();
            if (listavideojocs == null)
            {
                MessageBox.Show("no he trobat el fitxer!!!", "Atenció", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                for (int i = 0; i < listavideojocs.Count; i++)
                {
                    ListViewItem item = new ListViewItem();
                    item = listView1.Items.Add(listavideojocs[i].Identificador);
                    item.SubItems.Add(listavideojocs[i].Nom);
                    item.SubItems.Add(listavideojocs[i].Descripcio);
                    item.SubItems.Add(listavideojocs[i].Plataforma);
                    item.SubItems.Add(listavideojocs[i].Genere);
                    item.SubItems.Add(listavideojocs[i].Preu);
                    item.SubItems.Add(listavideojocs[i].FranjaEdat);
                    

                }
            
            }
            
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<Videojoc> listavideojocsPerGenere = new List<Videojoc>();
            listView1.Items.Clear();
            var itemSeleccionado = listBox1.SelectedItem.ToString();
            

            for (int i = 0; i < listavideojocs.Count; i++)
            {
                if (listavideojocs[i].Genere.Equals(itemSeleccionado))
                {
                    listavideojocsPerGenere.Add(listavideojocs[i]);                   
                }
            }

            for (int i = 0; i < listavideojocsPerGenere.Count; i++)
            {
                ListViewItem item = new ListViewItem();
                item = listView1.Items.Add(listavideojocsPerGenere[i].Identificador);
                item.SubItems.Add(listavideojocsPerGenere[i].Nom);
                item.SubItems.Add(listavideojocsPerGenere[i].Descripcio);
                item.SubItems.Add(listavideojocsPerGenere[i].Plataforma);
                item.SubItems.Add(listavideojocsPerGenere[i].Genere);
                item.SubItems.Add(listavideojocsPerGenere[i].Preu);
                item.SubItems.Add(listavideojocsPerGenere[i].FranjaEdat);
            }




        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<Videojoc> listavideojocsPerEdat = new List<Videojoc>();
            listView1.Items.Clear();

            var itemSeleccionado = comboBox1.SelectedItem.ToString();
            

            for (int i = 0; i < listavideojocs.Count; i++)
            {
                //si selecciona tots cargo la lista que muestra el listview con todos los elementos.
                if (itemSeleccionado.Equals("tots"))
                {
                    listavideojocsPerEdat.Add(listavideojocs[i]);
                }
                //si no cargo la lista del listview solo con los elementos la franja de edad seleccionada.
                else if (listavideojocs[i].FranjaEdat.Equals(itemSeleccionado))
                {
                    listavideojocsPerEdat.Add(listavideojocs[i]);

                }
                
                


            }
            for (int i = 0; i < listavideojocsPerEdat.Count; i++)
            {
                ListViewItem item = new ListViewItem();
                item = listView1.Items.Add(listavideojocsPerEdat[i].Identificador);
                item.SubItems.Add(listavideojocsPerEdat[i].Nom);
                item.SubItems.Add(listavideojocsPerEdat[i].Descripcio);
                item.SubItems.Add(listavideojocsPerEdat[i].Plataforma);
                item.SubItems.Add(listavideojocsPerEdat[i].Genere);
                item.SubItems.Add(listavideojocsPerEdat[i].Preu);
                item.SubItems.Add(listavideojocsPerEdat[i].FranjaEdat);
            }
        }

        private void FVideojocs_FormClosed(object sender, FormClosedEventArgs e)
        {
            Categorias categoriaForm = new Categorias();
            categoriaForm.Show();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            

            
        }

        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {

            if (detallsForm != null)
            {
                detallsForm.Close();
            }
            
            Videojoc videojoc;
            var videojocIndex = listView1.SelectedItems[0].SubItems[0].Text;
           

            for(int i = 0; i < listavideojocs.Count; i++)
            {
                if (listavideojocs[i].Identificador.Equals(videojocIndex))
                {
                    videojoc = new Videojoc(listavideojocs[i].Identificador,listavideojocs[i].Nom,listavideojocs[i].Descripcio,listavideojocs[i].Plataforma,listavideojocs[i].Genere,listavideojocs[i].Preu,listavideojocs[i].FranjaEdat);
                    detallsForm = new FDetalls(videojoc, listavideojocs);
                    detallsForm.ShowDialog();
                }
            }

             


           
        }
    }
}

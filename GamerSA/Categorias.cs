﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GamerSA
{
    public partial class Categorias : Form
    {
        CategoryADO catADO = new CategoryADO();
        
        private object b;
        public Categorias()
        {
            InitializeComponent();
            //cridar als metodes que vulguis que arrenquin 
            //al començament de tot
            loadingCategories();

            
        }

        private void Categorias_Load(object sender, EventArgs e)
        {
            
        }
        private void loadingCategories()
        {
            //llamar al modelo y en concreto al metodo loadCategories
            
            List<Category> allCats = new List<Category>();
            List<Button> allButtons = new List<Button>();
            allCats = catADO.loadCategories();//recollim

            if (allCats == null)
            {
                MessageBox.Show("No arribo al fitxer!!!", "Atenció", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                int i = 0;
                foreach (Category cat in allCats)//recorrem la llista
                {
                    Button b = new Button();//creacio
                    b.Text = cat.Name;//text del boto
                    

                    b.BackColor = Color.LightCoral;
                   


                    //posicio
                    b.Location = new System.Drawing.Point(150, 100 + i * 65);
                    //mida
                    b.Size = new System.Drawing.Size(180, 50);
                    b.Click += new System.EventHandler(clicantBotons);

                    Controls.Add(b);//aparezca en el formulario Form1
                    i++;
                    allButtons.Add(b);

                }

            }

        }

        private void clicantBotons(object sender, EventArgs e)
        {
            
            Button boto = (Button)sender;
            
            String opcion = boto.Text;

            string descripcion = catADO.obtenerDesc(opcion);

            if (!descripcion.Equals("videojocs"))
            {
                MessageBox.Show(descripcion, "En construccio ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                this.Hide();
                FVideojocs f4 = new FVideojocs();
                f4.Show();

            }
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Form2 f2 = new Form2();
            ////f2.Show();//formulario no modal: lo puedo poner en segunod plano
            //f2.ShowDialog();//formulario modal: primer plano
        }

        private void Categorias_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1 f3 = new Form1();
            f3.Show();

        }
    }
}


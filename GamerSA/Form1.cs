﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GamerSA
{
    public partial class Form1 : Form
    {
        UserDAO userDAO;
        

        public Form1()
        {
            InitializeComponent();
            userDAO = new UserDAO();
        }

        private void cargarDatos()
        {
           
        }

        private void btnValida_Click(object sender, EventArgs e)
        {

            string user = tbName.Text;
            string password = tbPassword.Text;
            string role = "";

            User u = new User(user, password);

            role = userDAO.validate(u);



            switch (role)
            {
                case "admin":
                    MessageBox.Show("Validacio Correcta!  (ADMIN)");
                    
                    Categorias f2 = new Categorias();
                    //f2.Show();//formulario no modal: lo puedo poner en segunod plano
                    f2.ShowDialog();//formulario modal: primer plano
                    this.Hide();
                    break;
                case "staff":
                    MessageBox.Show("Validació Correcta!  (STAFF)");
                    Categorias f3 = new Categorias();
                    //f2.Show();//formulario no modal: lo puedo poner en segunod plano
                    f3.ShowDialog();//formulario modal: primer plano                    
                    this.Hide();
                    //Avis admin = new Avis();
                    //admin.nameuser = u.name;
                    //admin.Show();
                    break;
                case null:
                    tbName.Text = "";
                    tbPassword.Text = "";
                    MessageBox.Show("Validacio incorrecta!");
                    tbName.Focus(); 
                    break;               
                default:
                    MessageBox.Show("Ha ocurrido un error inesperado");
                    //info.Text = "Ha ocurrido un error inesperado";
                    break;
            }

        }

        private void btnTancar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GamerSA
{
    public partial class FDetalls : Form
    {

        private Videojoc videojoc;


        public FDetalls()
        {

        }


        public FDetalls(Videojoc videojoc, List<Videojoc> listavideojocs)
        {
            InitializeComponent();

            this.videojoc = videojoc;

            string videojocID = videojoc.Identificador;
            try
            {
                for (int i = 0; i < listavideojocs.Count; i++)
                {
                    if (videojocID.Equals(listavideojocs[i].Identificador))
                    {
                        pictureBox1.Image = Image.FromFile("images/" + i + ".jpg");
                        lb_id.Text = listavideojocs[i].Identificador;
                        lb_nom.Text = listavideojocs[i].Nom;
                        lb_descripcio.Text = listavideojocs[i].Descripcio;
                        lb_plataforma.Text = listavideojocs[i].Plataforma;
                        lb_genere.Text = listavideojocs[i].Genere;
                        lb_preu.Text = listavideojocs[i].Preu;
                        lb_franjaEdat.Text = listavideojocs[i].FranjaEdat;
                    }
                    
                }
            }catch(Exception ex)
            {
                MessageBox.Show("Error no se pudo encontrar la imagen: "+ex.Message);
            }
            

            }


        private void FDetalls_Load(object sender, EventArgs e)
        {

        }

        private void lb_plataforma_Click(object sender, EventArgs e)
        {

        }
    }
}
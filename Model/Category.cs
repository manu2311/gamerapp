﻿using System;

namespace Model
{
    public class Category
    {
        //atributos
        private string name;
        private string description;

        //constructor
        public Category(string name, string description)
        {
            this.name = name;
            this.description = description;
        }

        //propiedades
        public string Name { 
            get => name; 
            set => name = value; 
        }
        public string Description { 
            get => description; 
            set => description = value;
        }
        
        
        /*public string getName()
{
return this.name;
}
public void setName(string value)
{
this.name = value;
}*/
    }
}

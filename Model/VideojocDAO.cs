﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class VideojocDAO
    {
        //se dedica a moverse por el fichero
        private string file = "videojocs.txt";
        private List<Videojoc> listaVideojocs = new List<Videojoc>();
        Videojoc videojoc;

        public List<Videojoc> loadVideojocs()
        {
            //abrir el fichero 
            try
            {
                String line;
                using (StreamReader sr = new StreamReader(file))
                {

                    while ((line = sr.ReadLine()) != null)
                    {
                        String[] pieces = line.Split(':');
                        videojoc = new Videojoc(pieces[0], pieces[1], pieces[2], pieces[3], pieces[4], pieces[5], pieces[6]);
                        listaVideojocs.Add(videojoc);

                    }
                    sr.Close();
                }
            }

            catch (FileNotFoundException errorFile)
            {
                
                Console.WriteLine("Error FileNotFound " + errorFile.Message);
                listaVideojocs = null;
            }
            catch (DirectoryNotFoundException errorDirectory)
            {
                Console.WriteLine("Error Directory " + errorDirectory.Message);
                listaVideojocs = null;

            }
            catch (IOException errorGeneralIO)
            {
                Console.WriteLine("Error General IO" + errorGeneralIO.Message);
                listaVideojocs = null;
            }
            catch (Exception error)
            {
                Console.WriteLine("Error General " + error.Message);
                listaVideojocs = null;
            }

            return listaVideojocs;
        }
    }

}

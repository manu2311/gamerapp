﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class UserDAO
    {
        List<User> users = new List<User>();


        public UserDAO()
        {
            cargaDatosEnLista();
        }


        public string validate(User u)
        {
            string role = null;

            foreach (var user in users)
            {
                if (user.name.Equals(u.name) && user.password.Equals(u.password))
                {
                    role = user.role;
                    break;
                }
                
            }
            return role;
        }

        private void cargaDatosEnLista()
        {
            string linea;
            string [] split;

            try
            {
                using (StreamReader sr = new StreamReader("users.txt"))
                {
                    while ((linea = sr.ReadLine()) != null)
                    {
                        split = linea.Split(':');

                        users.Add(new User(split[0], split[1], split[2]));
                    }
                }
            }
            catch (IOException ioe)
            {
            }
        }

    }
}

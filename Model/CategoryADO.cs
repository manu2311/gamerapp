﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Model
{
    //mètodes per accedir/manipular el fitxer
    public class CategoryADO
    {
        //se dedica a moverse por el fichero
        private string file = "categories.txt";
        private List<Category> cats = new List<Category>();
        Category cat;


        //uno de sus metodos: abrir el fichero y cargar las lineas en objetos Category

        public List<Category> loadCategories()
        {
            //abrir el fichero 
            try
            {
                String line;
                using(StreamReader sr=new StreamReader(this.file))
                {

                    while ((line=sr.ReadLine()) != null)
                    {
                        String[] pieces = line.Split(':');
                        cat = new Category(pieces[0], pieces[1]);
                        cats.Add(cat);
                        
                    }
                    sr.Close();
                }

            }
            catch (FileNotFoundException errorFile)
            {
                Console.WriteLine("Error FileNotFound "+errorFile.Message);
                cats = null;
            }catch(DirectoryNotFoundException errorDirectory)
            {
                Console.WriteLine("Error Directory " + errorDirectory.Message);
                cats = null;

            }
            catch (IOException errorGeneralIO)
            {
                Console.WriteLine("Error General IO" + errorGeneralIO.Message);
                cats = null;
            }
            catch(Exception error)
            {
                Console.WriteLine("Error General " + error .Message);
                cats = null;
            }
            return cats;

        }

        
        public string obtenerDesc(string opcion)
        {
            string descripcion = "";

            for (int i = 0; i < cats.Count; i++) {

                if (cats[i].Name.Equals(opcion))
                {

                    descripcion = cats[i].Description;

                }
                if (opcion.Equals("videojocs"))
                {
                    descripcion = opcion;
                }
                

            }
            return descripcion;
            
        }
    }
}

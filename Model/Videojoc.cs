﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Videojoc
    {

        //atributos
        private string identificador;
        private string nom;
        private string descripcio;
        private string plataforma;
        private string genere;
        private string preu;
        private string franjaEdat;

        //constructor
        public Videojoc(string identificador, string nom, string descripcio, string plataforma, string genere, string preu, string franjaEdat)
        {
            this.Identificador = identificador;
            this.Nom = nom;
            this.Descripcio = descripcio;
            this.Plataforma = plataforma;
            this.Genere = genere;
            this.Preu = preu;
            this.FranjaEdat = franjaEdat;                

        }

        //propiedades
       
        
        public string Identificador { get => identificador; set => identificador = value; }
        public string Nom { get => nom; set => nom = value; }
        public string Descripcio { get => descripcio; set => descripcio = value; }
        public string Plataforma { get => plataforma; set => plataforma = value; }
        public string Genere { get => genere; set => genere = value; }
        public string Preu { get => preu; set => preu = value; }
        public string FranjaEdat { get => franjaEdat; set => franjaEdat = value; }
    }
}

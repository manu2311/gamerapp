﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class User
    {
        public string name { get; set; }
        public string password { get; set; }
        public string role { get; set; }

        public User(string name, string password)
        {
            this.name = name;
            this.password = password;
        }

        public User(string name, string password, string role)
        {
            this.name = name;
            this.password = password;
            this.role = role;
        }
    }
}
